package com.pl.tt.plugin.weather.services.interfaces;

public interface EmployeeService {
    void updateDescription(boolean isUsername, boolean isEmail, String issueKey);
}
