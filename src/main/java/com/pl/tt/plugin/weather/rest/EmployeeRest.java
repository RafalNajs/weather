package com.pl.tt.plugin.weather.rest;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.pl.tt.plugin.weather.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Scanned
@Path("/employee")
public class EmployeeRest {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeRest(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage(@QueryParam("issueKey") String issueKey, @QueryParam("username") boolean isUsername, @QueryParam("email") boolean isEmail) {
        employeeService.updateDescription(isUsername, isEmail, issueKey);
        return Response.ok().build();
    }

}
