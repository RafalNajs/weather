AJS.$(document).ready(function () {
    var issueKey = AJS.$("#issue-key").val();

    AJS.$('#tt-do-something').on('click', function (event) {
        event.preventDefault();
        var username = AJS.$("#check-username").is(":checked");
        var email = AJS.$('#check-email').is(":checked");
        AJS.$.ajax({
            url: AJS.params.baseURL + '/rest/custom/latest/employee?issueKey=' + issueKey + '&username=' + username + '&email=' + email,
            type: 'GET',
            success: function (msg) {
                window.location.href = AJS.params.baseURL + '/browse/' + issueKey;
            }
        });
    });

    AJS.$('#tt-go-back').on('click', function (e) {
        e.preventDefault();
        window.location.href = AJS.params.baseURL + '/browse/' + issueKey;
    });
});