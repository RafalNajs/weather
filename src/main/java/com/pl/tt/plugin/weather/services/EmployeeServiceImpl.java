package com.pl.tt.plugin.weather.services;

import com.atlassian.jira.bc.customfield.CustomFieldService;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.UpdateValidationResult;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.pl.tt.plugin.weather.services.interfaces.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class EmployeeServiceImpl implements EmployeeService {
    private static String API_URL = "https://jsonplaceholder.typicode.com/users/";
    private static String JSON_MEMBER_USERNAME = "username";
    private static String CUSTOM_FIELD_NAME = "Employee Username";

    private final CustomFieldManager customFieldManager;
    private final IssueService issueService;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext authenticationContext;

    @Autowired
    public EmployeeServiceImpl(@ComponentImport CustomFieldManager customFieldManager,
                               @ComponentImport IssueService issueService,
                               @ComponentImport IssueManager issueManager,
                               @ComponentImport JiraAuthenticationContext authenticationContext) {
        this.customFieldManager = customFieldManager;
        this.issueService = issueService;
        this.issueManager = issueManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void updateDescription(boolean isUsername, boolean isEmail, String issueKey) {
        Optional<String> valueFromCustomField = getValueFromCustomField(CUSTOM_FIELD_NAME, issueKey);
        if(!valueFromCustomField.isPresent()){
            log.error("CustomField " + CUSTOM_FIELD_NAME + " is empty");
            return;
        }

        JsonArray clients = sendRequest();
        clients.forEach(client -> {
            if(client.getAsJsonObject().get(JSON_MEMBER_USERNAME).getAsString().toLowerCase()
                    .equals(valueFromCustomField.get().toLowerCase())) {

                String description = "";
                if(isEmail){
                    description = "Email: " + client.getAsJsonObject().get("email").toString() + "\n";
                }

                if (isUsername) {
                    description = description + "Username: " + client.getAsJsonObject().get("username").toString();
                }

                addDescription(description, issueKey);
            }
        });
    }

    private Optional<String> getValueFromCustomField(String customFieldName, String issueKey){
        IssueService.IssueResult issueResult = issueService.getIssue(authenticationContext.getLoggedInUser(), issueKey);
        if(!issueResult.isValid()){
           return Optional.empty();
        }

        Issue issue = issueResult.getIssue();

        List<CustomField> customFields = customFieldManager.getCustomFieldObjects(issue);
        Optional<CustomField> customField = customFields.stream().filter(cF -> cF.getName().toLowerCase().equals(customFieldName.toLowerCase())).findFirst();
        Optional<Object> value = customField.map(field -> field.getValue(issue));
        return value.map(Object::toString);
    }

    private JsonArray sendRequest() {
        HttpClient httpClient = HttpClientBuilder.create().build();
        JsonArray finalResult = new JsonArray();
        try {
            HttpGet httpGetRequest = new HttpGet(API_URL);
            HttpResponse httpResponse = httpClient.execute(httpGetRequest);
            BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            StringBuilder content = new StringBuilder();
            String line;
            while (null != (line = rd.readLine())) {
                content.append(line);
            }
            JsonParser parser = new JsonParser();
            Object obj= parser.parse(content.toString());
            finalResult = (JsonArray) obj;
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return finalResult;
    }

    private void addDescription(String response, String issueKey) {
        ApplicationUser loggedUser = authenticationContext.getLoggedInUser();
        MutableIssue issue = issueManager.getIssueByCurrentKey(issueKey);

        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        issueInputParameters.setDescription(response);

        UpdateValidationResult updateValidationResult = issueService.validateUpdate(loggedUser, issue.getId(), issueInputParameters);
        if(updateValidationResult.isValid()){
            issueService.update(loggedUser, updateValidationResult);
        } else {
            log.debug("Something went wrong\n");
        }
    }
}
