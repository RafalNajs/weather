package com.pl.tt.plugin.weather.servlets;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class EmployeeFormServlet extends HttpServlet{
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final TemplateRenderer templateRenderer;
    private final IssueManager issueManager;
    private final ProjectManager projectManager;

    @Inject
    public EmployeeFormServlet(
            @ComponentImport UserManager userManager,
            @ComponentImport LoginUriProvider loginUriProvider,
            @ComponentImport TemplateRenderer templateRenderer,
            @ComponentImport IssueManager issueManager,
            @ComponentImport ProjectManager projectManager) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.issueManager = issueManager;
        this.projectManager = projectManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = userManager.getRemoteUsername(req);
        if(username == null) {
            redirectToLogin(req, resp);
            return;
        }

        MutableIssue issue = issueManager.getIssueByCurrentKey(req.getParameter("key"));
        Project project = issue.getProjectObject();
        Map<String, Object> context = new HashMap<>();
        resp.setContentType("text/html;charset=utf-8");


        context.put("issue", issue);
        context.put("project", project);
        templateRenderer.render("templates/employeeForm.vm", context,  resp.getWriter());
    }

    private void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(loginUriProvider.getLoginUri(getUri(req)).toASCIIString());
    }

    private URI getUri(HttpServletRequest req) {
        StringBuffer builder = req.getRequestURL();
        if(req.getQueryString() != null) {
            builder.append("?");
            builder.append(req.getQueryString());
        }

        return URI.create(builder.toString());
    }

}